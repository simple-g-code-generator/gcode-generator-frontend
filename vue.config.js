const path = require("path");

module.exports = {
  outputDir: path.resolve(__dirname, "../backend/src/dist/"),
  assetsDir: "static/"
};
