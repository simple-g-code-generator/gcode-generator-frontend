import { extend, localize } from "vee-validate";
import {
  min_value,
  required,
  between,
  max_value
} from "vee-validate/dist/rules";
import en from "vee-validate/dist/locale/en.json";

// Install rules
extend("required", required);
extend("min_value", min_value);
extend("max_value", max_value);
extend("between", between);

// Install messages
localize({
  en
});
