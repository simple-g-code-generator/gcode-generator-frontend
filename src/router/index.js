import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import Ping from "../components/Ping.vue";
import NotFound from "../components/NotFound.vue";
import RectMill from "../components/RectMill.vue";
import CutMill from "../components/CutMill.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/about",
    name: "about",
    component: About
  },
  {
    path: "/ping",
    name: "ping",
    component: Ping
  },
  {
    path: "*",
    component: NotFound
  },
  {
    path: "/rectmill",
    name: "RectMill",
    component: RectMill
  },
  {
    path: "/cutmill",
    name: "CutMill",
    component: CutMill
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
